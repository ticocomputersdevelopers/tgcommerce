
/*======= FIXED HEADER ==========*/ 
var header = $('header'),
	header_height = $('header').height(),
	offset_top = $('header').offset().top,
	offset_bottom = offset_top + header.outerHeight(true),
	admin_menu = $('#admin-menu');


$(window).scroll(function(){
 
 	if ($(window).width() > 1024 ) {
 
	  	if ($(window).scrollTop() >= offset_bottom) {

	  		if(admin_menu.length){ $('.JSsticky_header').css('top', admin_menu.outerHeight() + 'px'); }

			$('#JSfixed_header').addClass('JSsticky_header');

			header.css('height', header_height + 'px');	   

			// STICKY CATEGORIES
			$('.JScategories').addClass('JSsticky-categories');
			$('.JSlevel-1').css('width', '270px');
			$('.categories-title').hide();
			$('.JSsticky-categories-title').show();
			$('#JSfixed_header').addClass('JSsticky-categories-header');
   

	    }else {

			$('#JSfixed_header').removeClass('JSsticky_header');    
			
			header.css('height', '');

			// STICKY CATEGORIES
			$('.JScategories').removeClass('JSsticky-categories');
			$('.categories-title').show();
			$('.JSsticky-categories-title').hide();
			$('#JSfixed_header').removeClass('JSsticky-categories-header');
			$('.JSlevel-1').css('width', '100%');
	    }
	}

	
});    
 
 