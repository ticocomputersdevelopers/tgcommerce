<?php

class AdminKursController extends Controller {

	function index($valuta_id) {

		$valute = DB::table('valuta')->where(['ukljuceno'=> 1,'izabran'=>0])->get();
		$current = DB::table('kursna_lista')->select('kupovni','srednji','prodajni','ziralni','web')->where(array('valuta_id'=>$valuta_id,'datum'=>date('Y-m-d')))->first();
		$tabela_valuta = DB::table('kursna_lista')->where('valuta_id',$valuta_id)->orderBy('kursna_lista_id', 'DESC')->get();
	
		$data=array(
            'strana' => 'kurs',
            'title' => 'Unos kursa',
            'valute' => $valute,
            'default' => DB::table('valuta')->where(['ukljuceno'=> 1,'izabran'=>1])->first(),
            'valuta_id' => $valuta_id,
            'tekuca_valuta' => isset($current) ? $current : (object) array('kupovni'=>'','srednji'=>'','prodajni'=>'','ziralni'=>'','web'=>''),
            'tabela_valuta' => $tabela_valuta
            );

		return View::make('admin/page', $data);
	}

	function edit() {
		$inputs = Input::get();
        $validator = Validator::make($inputs, array('kupovni' => 'numeric|digits_between:1,10','srednji' => 'numeric|digits_between:1,10','prodajni' => 'required|numeric|digits_between:1,10','ziralni' => 'required|numeric|digits_between:1,10','web' => 'required|numeric|digits_between:1,10'));
        if($validator->fails()){
            return Redirect::back()->with('alert','Polja nisu pravilno popunjena!');
        }else{
        	$current = DB::table('kursna_lista')->where(array('valuta_id'=>$inputs['valuta_id'],'datum'=>date('Y-m-d')))->first();

    		$data = array('kupovni'=>$inputs['kupovni'],'srednji'=>$inputs['srednji'],'prodajni'=>$inputs['prodajni'],'ziralni'=>$inputs['ziralni'],'web'=>$inputs['web']);
        	if(isset($current)){
                $kursna_lista_id = $current->kursna_lista_id;
                DB::table('kursna_lista')->where('kursna_lista_id',$kursna_lista_id)->update($data);
            }else{
                $data['ziralni'] = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');
                $data['web'] = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');
        		$data['kursna_lista_id'] = DB::table('kursna_lista')->max('kursna_lista_id')+1;
        		$data['valuta_id'] = $inputs['valuta_id'];
        		$data['datum'] = date('Y-m-d');
        		$data['paritet'] = 0;
        		DB::table('kursna_lista')->insert($data);
            }
            AdminSupport::saveLog('KURS_IZMENI', array($inputs['valuta_id']));
        	return Redirect::back()->with('message','Uspešno ste sačuvali podatke.');
        }
	}

	function kurs_nbs($id) {
        $content = file_get_contents('https://nbs.rs/kursnaListaModul/zaDevize.faces');
        //$exploded = explode('<td class="tableCell" style="text-align:right;">', $content);
        $euro = explode("<td>",explode("EUR",$content)[1]);
        $usd = explode("<td>",explode("USD",$content)[1]);
        $gbp = explode("<td>",explode("GBP",$content)[1]);
        $chf = explode("<td>",explode("CHF",$content)[1]);
        $bam = explode("<td>",explode("BAM",$content)[1]);
        $hrk = explode("<td>",explode("HRK",$content)[1]);

                $kursevi = array(
         'EUR' => array('kupovni' => '','kupovni' => floatval($euro[4]),'srednji' => (floatval($euro[4])+floatval($euro[5]))/2,'prodajni' => floatval($euro[5])),
         'USD' => array('kupovni' => '','kupovni' => floatval($usd[4]),'srednji' => (floatval($usd[4])+floatval($usd[5]))/2,'prodajni' => floatval($usd[5])),
         'GBP' => array('kupovni' => '','kupovni' => floatval($gbp[4]),'srednji' => (floatval($gbp[4])+floatval($gbp[5]))/2,'prodajni' => floatval($gbp[5])),
         'CHF' => array('kupovni' => '','kupovni' => floatval($chf[4]),'srednji' => (floatval($chf[4])+floatval($chf[5]))/2,'prodajni' => floatval($chf[5])),
         'BAM' => array('kupovni' => '','kupovni' => floatval($bam[4]),'srednji' => (floatval($bam[4])+floatval($bam[5]))/2,'prodajni' => floatval($bam[5])),
         'HRK' => array('kupovni' => '','kupovni' => floatval($hrk[4]),'srednji' => (floatval($hrk[4])+floatval($hrk[5]))/2,'prodajni' => floatval($hrk[5]))
         );

		$data = array('kupovni' => '','kupovni' => '','srednji' => '','prodajni' => '');

		$valuta_row = DB::table('valuta')->where('valuta_id',$id)->first();
		if(isset($valuta_row)){
			if(isset($kursevi[$valuta_row->valuta_sl])){
				$data = $kursevi[$valuta_row->valuta_sl];
			}else{
				return Redirect::to(AdminOptions::base_url().'admin/kurs/'.$id)->with('alert','Nije podržano preuzimanje ovog kursa!');
			}
		}

        $validator = Validator::make($data, array('kupovni' => 'numeric|digits_between:1,10','srednji' => 'numeric|digits_between:1,10','prodajni' => 'required|numeric|digits_between:1,10'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/kurs/'.$id)->with('alert','Došlo je do greške prilikom preuzimanja kursa!');
        }else{
        	$current = DB::table('kursna_lista')->where(array('valuta_id'=>$id,'datum'=>date('Y-m-d')))->first();

    		$data_arr = array('kupovni'=>$data['kupovni'],'srednji'=>$data['srednji'],'prodajni'=>$data['prodajni']);
        	if(isset($current)){
        		$kursna_lista_id = $current->kursna_lista_id;
        		DB::table('kursna_lista')->where('kursna_lista_id',$kursna_lista_id)->update($data_arr);
        	}else{
                $data_arr['ziralni'] = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');
                $data_arr['web'] = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('web');
        		$data_arr['kursna_lista_id'] = DB::table('kursna_lista')->max('kursna_lista_id')+1;
        		$data_arr['valuta_id'] = $id;
        		$data_arr['datum'] = date('Y-m-d');
        		$data_arr['paritet'] = 0;
        		DB::table('kursna_lista')->insert($data_arr);
        	}

			return Redirect::to(AdminOptions::base_url().'admin/kurs/'.$id)->with('message','Uspešno ste preuzeli kurs!');
        }

	}
}