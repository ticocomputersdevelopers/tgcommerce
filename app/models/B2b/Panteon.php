<?php

class Panteon {

    public static function createOrder($orderId){
        $order = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$orderId)->first();

        $cartItems=DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$orderId)->orderBy('broj_stavke','asc')->get();
        $nacin_isporuke = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$order->web_nacin_isporuke_id)->pluck('naziv');
        $nacin_placanja = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$order->web_nacin_placanja_id)->pluck('naziv');
        $note=substr('Način isporuke: '.$nacin_isporuke.'. Način plaćanja: '.$nacin_placanja.'. Napomena: '.$order->napomena,0,255);
        // $avans = (isset($order->web_nacin_placanja_id) && $order->web_nacin_placanja_id != 9) ? 0 : -4.1667;

        $success = false;
        $partner = DB::table('partner')->where('partner_id',$order->partner_id)->first();
        DB::connection('sqlsrv')->statement("set ansi_warnings on");
        DB::connection('sqlsrv')->statement("set concat_null_yields_null on");
        
        DB::connection('sqlsrv')->statement("set ANSI_PADDING on");
        // DB::connection('sqlsrv')->statement("EXEC _pB2BOrderCreate");
        if(!is_null($partner) && !is_null($partner->sifra)){
            // $troskovi = B2bBasket::troskovi();
            $results = self::createOrderDocumentPanteon($orderId,$partner,$note);

            foreach($cartItems as $key => $stavka){
                $roba = DB::table('roba')->where('roba_id',$stavka->roba_id)->first();
                if(!empty($roba->sifra_is)){
                    self::addOrderItemPanteon($orderId,$roba,$stavka,($key+1));
                }
            }

            //self::orderConfirmPanteon();
            $success = true;
        }
        return (object) array('success'=>$success, 'order_id'=>$orderId);
    }

    public static function createOrderDocumentPanteon($orderId,$partner,$note){
        $results = null;
        $db = DB::connection('sqlsrv');
        $pdo = $db->getPdo();
        DB::connection('sqlsrv')->statement("set ansi_warnings on");
        DB::connection('sqlsrv')->statement("set concat_null_yields_null on");
        
        DB::connection('sqlsrv')->statement("set ANSI_PADDING on");
        // DB::connection('sqlsrv')->statement("EXEC _pB2BOrderCreate");
        try {        
            $stmt = $pdo->query("declare @results char(13); EXEC pfsk_narudzbina '".strval($orderId)."','".strval($partner->sifra)."','".date('Ymd')."','".$note."'; select @results;");

            do {
                $rowset = $stmt->fetchAll(PDO::FETCH_NUM);
                if ($rowset) {
                    $results = $rowset;
                    // $results = isset($rowset[0]) && isset($rowset[0][0]) ? $rowset[0][0] : null;
                }        
            } while ($stmt->nextRowset());
        } catch (Exception $e) {
            echo $e->getMessage(); die;
        }

        return $results;
    }

    public static function addOrderItemPanteon($orderId,$article,$cartItem,$sortNumber){
        $napomena = '';
        $results = null;
        $db = DB::connection('sqlsrv');
        $pdo = $db->getPdo();

        $pdv = DB::table('tarifna_grupa')->where('tarifna_grupa_id',$article->tarifna_grupa_id)->pluck('porez');
        $jedinica_mere = DB::table('jedinica_mere')->where('jedinica_mere_id',$article->jedinica_mere_id)->pluck('naziv');

                DB::connection('sqlsrv')->statement("set ansi_warnings on");
        DB::connection('sqlsrv')->statement("set concat_null_yields_null on");
        
        DB::connection('sqlsrv')->statement("set ANSI_PADDING on");
        // DB::connection('sqlsrv')->statement("EXEC _pB2BOrderCreate");
        try {        
            $stmt = $pdo->query("declare @results char(13); EXEC pfsk_narudzbina_stavka '".strval($orderId)."', '".strval($article->id_is)."', '".strval($article->sifra_is)."', '".strval($cartItem->kolicina)."', '".strval($cartItem->jm_cena)."', '".strval($pdv)."', '".strval($jedinica_mere)."', '".strval($sortNumber)."', '".pg_escape_string($article->naziv_web)."'; select @results;");
            // $stmt = $pdo->query("EXEC pfsk_narudzbina_stavka '".strval($orderId)."', '".strval($article->id_is)."', '".strval($article->sifra_is)."', '".strval($cartItem->kolicina)."', '".strval($cartItem->jm_cena)."', '".strval($pdv)."', '".strval($jedinica_mere)."', '".strval($sortNumber)."', '".addslashes($article->naziv_web)."';");

            do {
                $rowset = $stmt->fetchAll(PDO::FETCH_NUM);
                if ($rowset) {
                    $results = $rowset;
                    // $itemOrder = isset($rowset[0]) && isset($rowset[0][0]) ? $rowset[0][0] : null;
                }        
            } while ($stmt->nextRowset());
        } catch (Exception $e) {
            echo $e->getMessage(); die;
        }

        return $results;
    }
    public static function orderConfirmPanteon(){
   
        DB::connection('sqlsrv')->statement("set ansi_warnings on");
        DB::connection('sqlsrv')->statement("set concat_null_yields_null on");
        
        DB::connection('sqlsrv')->statement("set ANSI_PADDING on");
        // DB::connection('sqlsrv')->statement("EXEC _pB2BOrderCreate");
      // DB::connection('sqlsrv')->statement("EXEC _pB2BOrderCreate");
   
    }

}