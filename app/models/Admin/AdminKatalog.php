<?php

	class AdminKatalog
	{
	    public static function find_katalog($katalog_id, $column)
	    {
	        return DB::table('katalog')->select($column)->where('katalog_id', $katalog_id)->pluck($column);
	    }
	    public static function getKatalogVrsta($active=null,$selected_id=null)
	        {
	        if($active){
	            return DB::table('katalog_vrsta')->select('katalog_vrsta_id','naziv')->where('katalog_vrsta_id','<>',-1)->where('katalog_vrsta_id','<>',0)->orderBy('naziv','asc')->get();
	        }else{
	            return DB::table('katalog_vrsta')->select('katalog_vrsta_id','naziv')->where('aktivan',1)->where('katalog_vrsta_id','<>',-1)->where('katalog_vrsta_id','<>',0)->orderBy('naziv','asc')->get();
	        }  
	    }
	    public static function getKatalog($active=null)
	    {
	        if($active){
	            return DB::table('katalog')->select('katalog_id','naziv')->where('katalog_id','<>',-1)->where('katalog_id','<>',0)->orderBy('naziv','asc')->get();
	        }else{
	            return DB::table('katalog')->select('katalog_id','naziv')->where('aktivan',1)->where('katalog_id','<>',-1)->where('katalog_id','<>',0)->orderBy('naziv','asc')->get();
	        }  
	    }

	    public static function getKatalogPolja($katalog_id){
	    	if($katalog_id==0){
	    		return array();
	    	}
	    	return DB::table('katalog_polja')->where('katalog_id',$katalog_id)->orderBy('rbr','asc')->get();
	    }

	    public static function getKatalogGrupeKarakteristikeSort($katalog_id){
	    	return DB::table('katalog_grupe_sort')
	    	->select('katalog_grupe_sort.*','grupa_pr.grupa as grupa_naziv','grupa_pr_naziv.naziv as karakteristika_naziv')
	    	->join('grupa_pr','katalog_grupe_sort.grupa_pr_id','=','grupa_pr.grupa_pr_id')
	    	->join('grupa_pr_naziv','katalog_grupe_sort.grupa_pr_naziv_id','=','grupa_pr_naziv.grupa_pr_naziv_id')->where('katalog_id',$katalog_id)
	    	->get();
	    }
	}

