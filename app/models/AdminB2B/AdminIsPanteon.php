<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);


use IsPanteon\DBData;
use IsPanteon\Article;
use IsPanteon\Stock;
use IsPanteon\Partner;
use IsPanteon\PartnerCard;
use IsPanteon\Support;


class AdminIsPanteon {

    public static function execute(){

        // /auto-import-is/13b80ca7f6c9af1087512c0427179fd5
       try {
            //articles
            $articles = DBData::articles();
            //$groups = Support::filteredGroups($articles);
           
            //Support::saveGroups($groups);

            foreach(Support::uniqueVats($articles) as $vatValue => $vatName){
                Support::getTarifnaGrupaId($vatName,$vatValue);
            }
            foreach(Support::uniqueMeasures($articles) as $measure){
                Support::getJedinicaMereId($measure);
            }

            $resultArticle = Article::table_body($articles);
            Article::query_insert_update($resultArticle->body,array('flag_cenovnik','flag_prikazi_u_cenovniku','flag_aktivan','racunska_cena_nc','web_cena','racunska_cena_end','mpcena','tarifna_grupa_id'));
            Article::query_update_unexists($resultArticle->body);
            $mappedArticles = Support::getMappedArticles();

            $stock = DBData::stock();
            $resultStock = Stock::table_body($stock,$mappedArticles);
            Stock::query_insert_update($resultStock->body);

            // Support::postUpdate();

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        }catch (Exception $e){
            AdminB2BIS::saveISLog('false');
            // AdminB2BIS::saveISLogError($e->getMessage());
            // AdminB2BIS::sendNotification(array(9,12,15,18),15,5);
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }

    public static function executePartnerFull(){

        // /auto-import-is-partner-full/13b80ca7f6c9af1087512c0427179fd5
        try {
            //partner
            $partners = DBData::partners();
            $resultPartner = Partner::table_body($partners);
            Partner::query_insert_update($resultPartner->body,array('sifra','naziv','adresa','rabat','racun','mesto'));
            $mappedPartners = Support::getMappedPartners();

            //partner card
            $partnersCards = DBData::partnersCards();            
            $resultPartnerCard = PartnerCard::table_body($partnersCards,$mappedPartners);
            PartnerCard::query_insert_delete($resultPartnerCard->body);


            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        }catch (Exception $e){
            AdminB2BIS::saveISLog('false');
            AdminB2BIS::saveISLogError($e->getMessage());
            AdminB2BIS::sendNotification(array(9,12,15,18),15,5);
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }

}