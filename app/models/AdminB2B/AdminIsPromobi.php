<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsPromobi\FileData;
use IsPromobi\Article;
use IsPromobi\Customer;
use IsPromobi\Seller;
use IsPromobi\PartnerSeller;
use IsPromobi\Stock;
use IsPromobi\PartnerCard;
use IsPromobi\Support;


class AdminIsPromobi {

    public static function execute(){

        try {
            FileData::readOrderStatuses();

            $customers = FileData::customers();
            $articleDetails = FileData::articles();
            // $partnersCards = FileData::partners_cards();

            //customer
            $resultCustomer = Customer::table_body($customers);
            Customer::query_insert_update($resultCustomer->body,array('naziv'));
            // $mappedPartners = Support::getMappedPartners();

            //seller
            $sellers = Support::getSellers($customers);
            $resultSeller = Seller::table_body($sellers);
            Seller::query_insert_update($resultSeller->body);
            $mappedSellers = Support::getMappedSellers();

            // //partner seller
            // $resultPartnerSeller = PartnerSeller::table_body($partners,$mappedSellers,$mappedPartners);
            // PartnerSeller::query_insert_update($resultPartnerSeller->body);

            //groups
            $groups = Support::filteredGroups($articleDetails->articles);
            Support::saveGroups($groups);
            
            //articles
            $resultArticle = Article::table_body($articleDetails->articles);
            Article::query_insert_update($resultArticle->body,array('flag_aktivan','flag_prikazi_u_cenovniku','sifra_is','grupa_pr_id','jedinica_mere_id','racunska_cena_nc','racunska_cena_end','mpcena','web_cena'));
            // Article::query_update_unexists($resultArticle->body);
            $mappedArticles = Support::getMappedArticles();

            //stocl
            $resultStock = Stock::table_body($articleDetails->articles,$mappedArticles,1);
            Stock::query_insert_update($resultStock->body);


            // //partner card
            // $resultPartnersCards = PartnerCard::table_body($partnersCards,$mappedPartners);
            // PartnerCard::query_insert_update($resultPartnersCards->body);


            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        }catch (Exception $e){
            AdminB2BIS::saveISLog('false');
            AdminB2BIS::sendNotification(array(9,12,15,18),9,2);
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }



}