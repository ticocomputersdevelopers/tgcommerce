<?php
namespace IsPanteon;

use DB;


class DBData {

	public static function articles(){
		return DB::connection('sqlsrv')->select("SELECT anQid, acIdent, acName, anVat, acUm, anRTprice, anSalePrice, acCurrency, acDescr FROM _b2bSetItem GROUP BY anQid, acIdent, acName, anVat, acUm, anRTprice, anSalePrice, acCurrency, acDescr");
	}
	public static function stock(){
		return DB::connection('sqlsrv')->select("SELECT anQid, anStock, * FROM _b2bSetItem where anQid is not null");
	}

	public static function partners(){
		return DB::connection('sqlsrv')->select("SELECT * FROM _b2bSetSubj");
	}

	public static function partnersCards(){
		return DB::connection('sqlsrv')->select("SELECT * FROM _b2bAcctTransItem");
	}

}