<?php
namespace IsPanteon;
use DB;

class Stock {
	public static function table_body($articles,$mapped_articles){
  //       $groupedStocks = Support::groupedStocks($articles);
  //       foreach($groupedStocks as $stockroom){
  //           Support::getOrgjId($stockroom);
  //       }
		// $mappedStockrooms = Support::getMappedStockrooms();

		$result_arr = array();
		foreach($articles as $article) {
			$sifra = $article->anQid;
			// $stockCode = trim($article->acWarehouse);
			$roba_id = isset($mapped_articles[strval($sifra)]) ? $mapped_articles[strval($sifra)] : null;
			// $orgj_id = isset($mappedStockrooms[strval($stockCode)]) ? $mappedStockrooms[strval($stockCode)] : null;
			$orgj_id = 4;

			if(!is_null($roba_id) && !is_null($orgj_id)){
				$kolicina = 0;
				if(!empty($article->anStock) && is_numeric($article->anStock) && $article->anStock > 0){
					$kolicina = $article->anStock;
				}

				$result_arr[] = "(".strval($roba_id).",".strval($orgj_id).",0,0,0,".strval($kolicina).",0,0,0,0,0,0,0,0,(NULL)::integer,0,0,0,0,0,2014,-1,0,0,'".strval($sifra)."')";
			}

		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='lager'"));
		$table_temp = "(VALUES ".$table_temp_body.") lager_temp(".implode(',',$columns).")";

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="sifra_is"){
		    	$updated_columns[] = "".$col." = lager_temp.".$col."";
			}
		}

		DB::statement("UPDATE lager t SET kolicina = lt.kolicina FROM (select sum(kolicina) as kolicina, min(roba_id) as roba_id, min(orgj_id) as orgj_id, min(poslovna_godina_id) as poslovna_godina_id from ".$table_temp." group by lager_temp.roba_id, lager_temp.orgj_id, lager_temp.poslovna_godina_id) lt WHERE t.roba_id=lt.roba_id AND t.orgj_id=lt.orgj_id AND t.poslovna_godina_id=lt.poslovna_godina_id");

		$where = "";
		if(DB::table('lager')->count() > 0){
			$where .= " WHERE (roba_id, orgj_id, poslovna_godina_id) NOT IN (SELECT roba_id, orgj_id, poslovna_godina_id FROM lager)";
		}

		//insert
		DB::statement("INSERT INTO lager (roba_id, orgj_id, kolicina, poslovna_godina_id) SELECT roba_id, orgj_id, SUM(kolicina), poslovna_godina_id FROM ".$table_temp.$where." GROUP BY roba_id, orgj_id, poslovna_godina_id");

		//delete
		DB::statement("DELETE FROM lager WHERE (roba_id, orgj_id, poslovna_godina_id) NOT IN (SELECT roba_id, orgj_id, poslovna_godina_id FROM ".$table_temp.")");

	}

}