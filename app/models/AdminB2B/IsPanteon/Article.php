<?php
namespace IsPanteon;
use DB;
use Options;

class Article {

	public static function table_body($articles){
		$mappedVats = Support::getMappedVats();
		$mappedMeasures = Support::getMappedMeasures();
		$mappedGroups = Support::getMappedGroups();

		$ostalo_grupa_pr_id = Support::getNewGrupaId('NOVI ARTIKLI');
		
		$kurs = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');
		$mappedPrices = Support::getMappedPrices();

		$result_arr = array();
		$roba_id =  DB::table('roba')->max('roba_id')+1;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;

		foreach($articles as $article) {
			if(!empty($article->anQid)){
				$id_is = $article->anQid;
				$roba_id++;
				$sifra_k++;

				$sifra_is = !empty($article->acIdent) ? pg_escape_string($article->acIdent) : '';
				$naziv = !empty($article->acName) ? substr($article->acName,0,300) : '';
				$naziv = pg_escape_string(Support::convertEntity(str_replace(['&#262;', '&#263;', '&#352;', '&#353;', '&#268;', '&#269;', '&#272;', '&#273;', '&#381;', '&#382;', '&#956;'], ['Ć', 'ć', 'Š', 'š', 'Č', 'č', 'Ð', 'đ', 'Ž', 'ž', 'μ'], $naziv)));
				$web_opis = !empty($article->acDescr) && $article->acDescr != 'opis' ? Support::convert($article->acDescr) : '';

				$jedinica_mere_id = !empty(trim($article->acUm)) && isset($mappedMeasures[strval(trim($article->acUm))]) ? $mappedMeasures[strval(trim($article->acUm))] : 1;
				$vrednost_tarifne_grupe = !empty($article->anVat) && is_numeric($article->anVat) ? intval($article->anVat) : 20;
				$tarifna_grupa_id = !empty($mappedVats[$vrednost_tarifne_grupe]) ? $mappedVats[$vrednost_tarifne_grupe] : 0;
				$proizvodjac_id = -1;

				// $grupa_pr_id = !empty($article->acClassif) && trim($article->acClassif) != '' && !empty($article->acClassif2) && trim($article->acClassif2) != '' && isset($mappedGroups[trim($article->acClassif2).'<=>'.trim($article->acClassif)]) ? $mappedGroups[trim(mb_convert_encoding($article->acClassif2,mb_detect_encoding($article->acClassif2),"UTF-8")).'<=>'.trim(mb_convert_encoding($article->acClassif,mb_detect_encoding($article->acClassif),"UTF-8"))] : $ostalo_grupa_pr_id;
				// $barkod = !is_null($article->acCode) && $article->acCode != '' ? "'".Support::convert(strval($article->acCode))."'" : "NULL";
				$grupa_pr_id = $ostalo_grupa_pr_id;
				$barkod = "NULL";
				$kursValuta = trim($article->acCurrency) != 'EUR' ? 1 : $kurs;

				$racunska_cena_nc = 0;
				$mpcena = !empty($article->anSalePrice) && is_numeric(floatval($article->anSalePrice)) ? (floatval($article->anSalePrice) * $kursValuta) : 0;
				$web_cena = $mpcena;
				$racunska_cena_a = !empty($article->anRTprice) && is_numeric(floatval($article->anRTprice)) ? (floatval($article->anRTprice) * $kursValuta) : 0;
				$racunska_cena_end = !empty($article->anRTprice) && is_numeric(floatval($article->anRTprice)) ? (floatval($article->anRTprice) * $kursValuta) : 0;

				// $stara_cena = !empty($mappedPrices[$id_is]) ? ($mappedPrices[$id_is]->racunska_cena_end != $racunska_cena_end ? $mappedPrices[$id_is]->racunska_cena_end : $mappedPrices[$id_is]->stara_cena) : $racunska_cena_end;
				$stara_cena = $racunska_cena_end;

				$marza = 0;
				$flag_aktivan = $racunska_cena_end > 0 ? '1' : '0';
				$flag_cenovnik = $racunska_cena_end > 0 ? '1' : '0';
				// $sku = !empty($article->ProductNumber) && !empty(preg_replace('/\s+/', '', $article->ProductNumber)) ? "'".pg_escape_string(preg_replace('/\s+/', '', $article->ProductNumber))."'" : "NULL";
				// $garancija = !empty($article->Warranty) && is_numeric($article->Warranty) ? $article->Warranty : "0";
				$sku = "NULL";
				$garancija = "0";

				$result_arr[] = "(".strval($roba_id).",NULL,'".$naziv."',NULL,NULL,NULL,".$grupa_pr_id.",".strval($tarifna_grupa_id).",".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,'".substr($naziv,0,20)."',0,-1,0,0,0,0,9,0,0,0,".strval($garancija).",1,".$flag_cenovnik.",0,NULL,".$flag_aktivan.",".strval($racunska_cena_nc).",".strval($racunska_cena_a).",".strval($racunska_cena_end).",0,NULL,".strval($mpcena).",false,".strval($marza).",(NULL)::integer,'".$naziv."',".$flag_cenovnik.",NULL,('".date('Y-m-d H:i:s')."')::timestamp,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,'".$web_opis."',NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,0,".$barkod.",".strval($stara_cena).",0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,0.00,0,".$sku.",(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra_is)."','".strval($id_is)."',0,(NULL)::integer,0,(NULL)::date,(NULL)::date,0.00,0.00,1,NULL)";
			}
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}

		$all_columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$columns = $all_columns;
		$columns_without_id = $all_columns;
		unset($columns_without_id[0]);
		
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		//DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		
		// update
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="sifra_d" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}

		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar AND t.id_is IS NOT NULL AND t.id_is <> '' AND t.flag_zakljucan='false'");	
		//insert
		DB::statement("INSERT INTO roba(".implode(',',$columns_without_id).") SELECT ".implode(',',$columns_without_id)." FROM ".$table_temp." WHERE roba_temp.id_is NOT IN (SELECT id_is FROM roba WHERE id_is IS NOT NULL AND id_is <> '')");


		//DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	   // DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba) + 1, FALSE)");
	}

	public static function query_update_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		DB::statement("UPDATE roba t SET flag_aktivan = 0 WHERE t.id_is NOT IN (SELECT id_is FROM ".$table_temp.")");
	}

}