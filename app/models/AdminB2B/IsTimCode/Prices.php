<?php
namespace IsTimCode;
use AdminB2BIS;
use DB;

class Prices {

	public static function table_body($prices,$mappedArticles){
		$result_arr = array();

		$sifra_k = DB::table('roba')->max('sifra_k')+1;

		foreach($prices as $price) {
			$id_is = $price->id;
  			$price = $price->attributes;

			$roba_id = isset($mappedArticles[trim(strval($id_is))]) ? $mappedArticles[trim(strval($id_is))] : null;

			if(!is_null($roba_id)){
				$sifra_k++;
				$sifra_is = '';
				$grupa_pr_id = -1;
				$naziv = '';
				$jedinica_mere_id = 0;
				$proizvodjac_id = -1;
				$racunska_cena_nc = 0;
				$web_cena = 0;
				$barkod = '';
				$tarifna_grupa_id = 0;
				$flag_prikazi_u_cenovniku = '1';
				$flag_aktivan = '1';
				$akcija = 0;

				$mpcena = floatval($price->cena);

				$result_arr[] = "(".strval($roba_id).",NULL,'".$naziv."',NULL,NULL,NULL,".$grupa_pr_id.",".strval($tarifna_grupa_id).",".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,'".substr($naziv,0,20)."',0,-1,0,0,0,0,9,0,0,0,0,1,".$flag_prikazi_u_cenovniku.",0,NULL,".$flag_aktivan.",".strval($racunska_cena_nc).",0,".strval($racunska_cena_nc).",0,NULL,".strval($mpcena).",false,0,(NULL)::integer,'".$naziv."',1,NULL,NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,NULL,NULL,NULL,0,".strval($akcija).",0,NULL,NULL,NULL,NULL,1,0,'".$barkod."',0,0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,0.00,0,'".strval($sifra_is)."',(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra_is)."','".strval($id_is)."',0,0)";
			}

		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}

		$all_columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$columns = $all_columns;
		$columns_without_id = $all_columns;
		unset($columns_without_id[0]);
		
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		
		// update
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="sifra_d" && $col!="sifra_is" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}

		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar AND t.id_is IS NOT NULL AND t.id_is <> ''");

	}

}