<?php
namespace IsLogik;
use DB;

class MpArticle {

	public static function table_body($articles,$mapped_articles){
		$result_arr = array();
		$dobavljac_cenovnik_id =  DB::table('dobavljac_cenovnik')->max('dobavljac_cenovnik_id')+1;

		foreach($articles as $article) {
			$roba_id = isset($mapped_articles[$article->sifra_artikla_logik]) ? $mapped_articles[$article->sifra_artikla_logik] : null;

			if(!is_null($roba_id)){
				$dobavljac_cenovnik_id++;

				$id_is = $article->sifra_artikla_logik;
				$sifra_is = $article->kod_artikla_logik;
				$barkod = pg_escape_string(substr($article->barkod,0,30));
				$naziv = pg_escape_string(Support::convert(substr($article->naziv_artikla,0,300)));
				$web_opis = pg_escape_string(nl2br(htmlspecialchars(Support::convert($article->opis))));
				$flag_opis_postoji = !is_null($web_opis) && $web_opis != '' ? '1' : '0';
				$model = pg_escape_string(substr($article->model,0,300));
				// $jedinica_mere_id = Support::getJedinicaMereId($article->naziv_jedinice_mere);
				$proizvodjac = substr($article->naziv_proizvodjaca,0,100);
				// $tarifna_grupa_id = Support::getTarifnaGrupaId($article->naziv_tarifne_grupe,$article->vrednost_tarifne_grupe);
				$tarifna_grupa_id = 0;

				// $flag_aktivan = $article->aktivan == 'y' ? '1' : '0';
				$flag_aktivan = '1';
				$grupa = substr($article->naziv_kategorije,0,100);
				$nadgrupa = substr($article->nadkategorija,0,100);
				$racunska_cena_nc = intval($article->nabavna_cena);
				// $mpcena = intval($article->maloprodajna_cena); //(1+intval($article->porez)/100)
				$mpcena = 0;
				$web_cena = 0;
				$preporucena_cena = intval($article->preporucena_cena);
				$kolicina = intval($article->mp_lager);

				$result_arr[] = "(".$dobavljac_cenovnik_id.",1,'".$sifra_is."',".$roba_id.",1,'".$naziv."',20,".strval($tarifna_grupa_id).",'".$nadgrupa."','".$grupa."',-1,'".$proizvodjac."',-1,".strval($kolicina).",0,".strval($racunska_cena_nc).",0,0,0,0,0,".strval($mpcena).",0,".strval($web_cena).",0,1,".$flag_aktivan.",'".$web_opis."',0,'".$model."',1,NULL,NULL,".$flag_opis_postoji.",0,0,'".$barkod."',".$preporucena_cena.",0,0,NULL,0,NULL)";
			}
		}
		
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='dobavljac_cenovnik'"));
		$table_temp = "(VALUES ".$table_temp_body.") dobavljac_cenovnik_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		
		// update
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="dobavljac_cenovnik_id" && $col!="roba_id" && $col!="sifra_kod_dobavljaca"){
		    	$updated_columns[] = "".$col." = dobavljac_cenovnik_temp.".$col."";
			}
		}

		DB::statement("UPDATE dobavljac_cenovnik t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.sifra_kod_dobavljaca=dobavljac_cenovnik_temp.sifra_kod_dobavljaca::varchar AND t.partner_id = 1");
		//insert
		DB::statement("INSERT INTO dobavljac_cenovnik (SELECT * FROM ".$table_temp." WHERE dobavljac_cenovnik_temp.sifra_kod_dobavljaca NOT IN (SELECT sifra_kod_dobavljaca FROM dobavljac_cenovnik))");


		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

		DB::statement("SELECT setval('dobavljac_cenovnik_dobavljac_cenovnik_id_seq', (SELECT MAX(dobavljac_cenovnik_id) FROM dobavljac_cenovnik) + 1, FALSE)");
	}

	public static function query_update_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='dobavljac_cenovnik'"));
		$table_temp = "(VALUES ".$table_temp_body.") dobavljac_cenovnik_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("UPDATE dobavljac_cenovnik t SET flag_aktivan = 0 WHERE partner_id = 1 AND t.sifra_kod_dobavljaca IS NOT NULL AND NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.sifra_kod_dobavljaca=dobavljac_cenovnik_temp.sifra_kod_dobavljaca::varchar)");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

}