<?php

class Languages extends Eloquent
{
    protected $table = 'jezik';

    protected $primaryKey = 'jezik_id';
    
    public $timestamps = false;

	protected $hidden = array(
        'jezik_id',
        'naziv',
        'kod',
        'aktivan',
        'izabrani',
        'putanja_slika',
        'admin_izabrani'
    	);

	public function getIdAttribute()
	{
	    return isset($this->attributes['jezik_id']) ? $this->attributes['jezik_id'] : null;
	}
    public function getNameAttribute()
    {
        return isset($this->attributes['naziv']) ? $this->attributes['naziv'] : null;
    }
    public function getCodeAttribute()
    {
        return isset($this->attributes['kod']) ? $this->attributes['kod'] : null;
    }
    public function getActiveAttribute()
    {
        return isset($this->attributes['aktivan']) ? $this->attributes['aktivan'] : null;
    }
    public function getSelectedAttribute()
    {
        return isset($this->attributes['izabrani']) ? $this->attributes['izabrani'] : null;
    }
    public function getImagePathAttribute()
    {
        return isset($this->attributes['putanja_slika']) ? $this->attributes['putanja_slika'] : null;
    }
    public function getAdminSelectedAttribute()
    {
        return isset($this->attributes['admin_izabrani']) ? $this->attributes['admin_izabrani'] : null;
    }

	protected $appends = array(
    	'id',
        'name',
        'code',
        'active',
        'selected',
        'image_path',
        'admin_selected'
    	);

    public function categories(){
        return $this->hasMany('Category','grupa_pr_id');
    }

}