<?php 


class Partner extends Eloquent {
    
    protected $table = 'partner';
    
    public function isPartner(){
        return Auth::user()->partner_id>0;    
    }

    public static function getPartnerName(){
        $partner = Partner::where('partner_id',Session::get('b2b_user_'.Options::server()))->first();
        return $partner->naziv;
    }

    public static function forgotPassword($mail){
        $newPassword = All::user_password();
        DB::table('partner')->where('mail',$mail)->update(array('password' => $newPassword));
        $body="Vi ili neko sa Vašom e-mail adresom ".$mail." je uputio zahtev sa <a href='".Options::base_url()."b2b'>".Options::base_url()."b2b</a> u vezi sa zaboravljenom lozinkom.Vaša lozinka je resetovana i glasi ovako:<br />".$newPassword;
        $subject="Promena lozinke";
        WebKupac::send_email_to_client($body,$mail,$subject);

    }


} 