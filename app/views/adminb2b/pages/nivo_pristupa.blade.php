@extends('adminb2b.defaultlayout')
@section('content')

<section class="" id="main-content">
	
	<div class="row">
		@include('adminb2b.partials.nivo_pristupa_tabs')
		@include('adminb2b.pages.'.$strana.'')
	</div>
	
</section> 
@endsection