@extends('adminb2b.defaultlayout')
@section('content')

<div id="main-content">
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif
	@if(Session::has('error_message'))
	<script>
		alertify.error('{{ Session::get('error_message') }}');
	</script>
	@endif
	<div class="row">
		<div class="column medium-12">
			<h3 class="title-med"> {{ AdminLanguage::transAdmin('Unesite željeni css') }}  </h3>
		</div>

		<div class="column medium-12 margin-h-10">
			{{ AdminLanguage::transAdmin('Uneti stilovi u editoru će biti sačuvani u eksterni CSS fajl <span class="css_info">client_custom_b2b.css</span>, uključen u <span class="css_info">&lt;head&gt;</span> tag. Novi CSS će imati prednost u odnosu na postojeći.') }}
		</div>

		<form method="POST" action="{{ AdminB2BOptions::base_url() }}admin/b2b/css_save" enctype="multipart/form-data" class="column medium-7">
			<input type="hidden" name="custom_css_id" value="2">
			<textarea name="sadrzaj" class="ace_editor" value="{{$custom_css[0]->sadrzaj}}">{{$custom_css[0]->sadrzaj}}</textarea> 
			<br>
			<label class="inline-block"> {{ AdminLanguage::transAdmin('Aktivan') }} </label>
			<input name="flag_aktivan" type="checkbox" value="1" {{$flag_aktivan == 1 ? 'checked' : ''}} >				
			<button type="submit" class="btn save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
			<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminB2BOptions::base_url() }}admin/b2b/custom_css_delete/{{$custom_css_id}}">{{ AdminLanguage::transAdmin('Obriši') }}</button>
		</form>
	</div>
</section>
@endsection