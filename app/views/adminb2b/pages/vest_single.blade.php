@extends('adminb2b.defaultlayout')
@section('content')
<section id="main-content">
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif
	<div class="row m-subnav">
		<div class="large-2 medium-3 small-12 columns ">
			<a class="m-subnav__link" href="{{AdminB2BOptions::base_url()}}admin/b2b/vesti" >
				<div class="m-subnav__link__icon">
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
				</div>
				<div class="m-subnav__link__text">
					Nazad
				</div>
			</a>
		</div>
	</div>
	<div class="row"> 
		<div class="column medium-10 medium-centered large-centered"> 
			<form action="{{AdminB2BOptions::base_url()}}admin/b2b/vesti/{{ $web_vest_b2b_id }}/save" class="flat-box" method="post" enctype="multipart/form-data">
				<input type="hidden" name="id" value="{{ $web_vest_b2b_id }}">
				<input type="hidden" name="jezik_id" value="{{ $jezik_id }}">
				<div class="row"> 
					<div class="columns medium-12 field-group{{ $errors->first('naslov') ? ' error' : '' }}">
					<label for="">Naslov</label>
					<input class="news-single-title" type="text" name="naslov" value="{{ htmlentities(!is_null(Input::old('naslov')) ? Input::old('naslov') : $naslov) }}" {{ Admin_model::check_admin(array('VESTI_AZURIRANJE')) == false ? 'readonly' : '' }}>
					<a class="btn btn-primary btn-secondary" href="{{ AdminB2BVesti::news_link_b2b($web_vest_b2b_id) }}" target="_blank">Vidi vest</a>
					</div>
				</div>
				<div class="row"> 
					<div class="columns medium-2 field-group{{ $errors->first('rbr') ? ' error' : '' }}">
						<label for="">Redni broj</label>
						<input class="ordered-number" type="text" name="rbr" value="{{ !is_null(Input::old('rbr')) ? Input::old('rbr') : $rbr }}" {{ Admin_model::check_admin(array('VESTI_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>
					<div class="columns medium-12 field-group">
						@if($web_vest_b2b_id != 0)
						<img class="news-pic" src="{{ AdminB2BOptions::base_url() . $slika }}">
						@endif
						@if(Admin_model::check_admin(array('VESTI_AZURIRANJE')))
							<div class="input-group">
								<div class="bg-image-file">
									<label>Dodaj sliku</label>
									<input type="file" name="news_img"> 
								</div>
							</div>
						@endif
					</div>
				</div>
				@if(Admin_model::check_admin(array('VESTI_AZURIRANJE')))
					@if(count($jezici) > 1)
					<div class="row"> 
						<div class="languages">
							<ul>	
								@foreach($jezici as $jezik)
								<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminB2BOptions::base_url()}}admin/b2b/vest/{{$web_vest_b2b_id}}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
								@endforeach
							</ul>
						</div>
					</div>
					@endif
				@endif
				<div class="row"> 
					<div class="columns medium-12 field-group{{ $errors->first('tekst') ? ' error' : '' }}">
						<textarea @if(Admin_model::check_admin(array('VESTI_AZURIRANJE'))) class="special-textareas" @endif name="tekst" id="content" {{ Admin_model::check_admin(array('VESTI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ !is_null(Input::old('tekst')) ? Input::old('tekst') : $tekst }}</textarea>
					</div>
				</div>
				<div class="row"> 
					<div class="columns medium-12 field-group{{ $errors->first('seo_title') ? ' error' : '' }}">
						<label>SEO title</label>
						<input type="text" name="seo_title" value="{{ htmlentities(Input::old('seo_title') ? Input::old('seo_title') : $seo_title) }}" onkeydown=" return character_limit(event,60)" {{ Admin_model::check_admin(array('VESTI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
					</div>
				</div>
				<div class="row"> 
					<div class="columns medium-12 field-group{{ $errors->first('description') ? ' error' : '' }}">
						<label for="description">SEO opis</label>
						<input type="text" name="description" onkeydown=" return character_limit(event)"
						value="{{ Input::old('description') ? Input::old('description') : $description }}" {{ Admin_model::check_admin(array('VESTI_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>
				</div>	
				<div class="row"> 
					<div class="columns medium-12 field-group{{ $errors->first('keywords') ? ' error' : '' }}">
						<label for="keywords">SEO keywords</label>
						<input type="text" name="keywords" onkeydown="return character_limit(event,159)"
						value="{{ Input::old('keywords') ? Input::old('keywords') : $keywords }}" {{ Admin_model::check_admin(array('VESTI_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>
				</div>
				<div class="row">
					<div class="columns medium-12">
						<input type="checkbox" name="aktuelno" {{ $aktuelno == 1 ? 'checked' : '' }} {{ Admin_model::check_admin(array('VESTI_AZURIRANJE')) == false ? 'disabled' : '' }}>Aktivna vest			   		
					</div>
				</div>
				@if(Admin_model::check_admin(array('VESTI_AZURIRANJE')))
					<div class="row">
						<div class="columns medium-12 large-12">
							<div class="btn-container text-center"> 
								<button class="btn btn-large news-save save-it-btn">Sačuvaj</button>
								@if($web_vest_b2b_id != 0)
								<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminB2BOptions::base_url() }}admin/b2b/vesti/{{ $web_vest_b2b_id }}/delete">Obriši</button>
								@endif
							</div>
						</div>  
					</div>
				@endif			
			</form>
		</div>	
	</div>
	<!-- 	<div class="row">
	<div class="image-upload-area single-news-upload clearfix medium-12 large-10 large-centered columns">

	<form method="post" enctype="multipart/form-data" action="{{AdminB2BOptions::base_url()}}admin/b2b/image_upload" >
	<label>Dodajte sliku na strani:</label>

	<input type="file" id="img" name="img" class="file-input">
	<button class="file-upload btn btn-secondary" type="submit">Upload</button>
	</form>
	<div class="images_upload has-tooltip" title='Prevucite sliku na željenu poziciju'>
	{{Admin_model::upload_directory()}}
	</div>
	</div>
	</div> -->

</section>
@endsection