	<div class="row"> 
		<div class="logo col-3">
			<img src="{{ AdminOptions::base_url()}}{{$podesavanja->logo}}" alt="logo">
		</div>
		<div class="col-4 company-info">
			<p class="comp-name">{{AdminOptions::company_name()}}</p>
			<p>{{AdminOptions::company_adress()}}, {{AdminOptions::company_mesto()}}</p>
			<p>Telefon: {{AdminOptions::company_phone()}}, Fax: {{AdminOptions::company_fax()}}</p>
			<p>PIB: {{AdminOptions::company_pib()}}</p>
		    <p>E-mail: {{AdminOptions::company_email()}}</p>
		</div>
		
		<div class="col-4 kupac-info">
		<table>
			<tr>
	            <td>Kupac:</td>
	            <td>{{$partner->naziv}}</td>
	        </tr>
	        <tr>
	            <td>PIB:</td>
	            <td>{{$partner->pib}}</td>
	        </tr>
            <tr>
	            <td>Adresa:</td>
	            <td>{{$partner->adresa}}</td>
            </tr>
            <tr>
	            <td>Mesto:</td>
	            <td>{{$partner->mesto}}</td>
            </tr>
            <tr>
	            <td>Telefon:</td>
	            <td>{{$partner->telefon}}</td>
            </tr>
            <tr>
	            <td>Email:</td>
	            <td>{{$partner->mail}}</td>
            </tr>
        </table>
		</div>
 	</div>		

 	<div class="row"> 
		<p class="ziro">Žiro račun: {{AdminOptions::company_ziro()}}</p>
	 </div>

 	<div class="row">
		<h4 class="racun-br">Račun broj: {{$racun->broj_dokumenta}}</h4>
	 </div>

	<div class="row"> 
		<table class="info-1">
			<thead>
				<tr>
					<td>Mesto i datum izdavanja računa</td>
					<td>Mesto i datum prometa dobara i usluga</td>
					<td>Rok isporuke</td>
					<td>Valuta plaćanja</td>
					<td>Rok plaćanja</td>
					<td>Način plaćanja</td>
				</tr>
			</thead>

			<tbody>
				<tr>						
					<td>{{AdminOptions::company_mesto()}},{{AdminNarudzbine::formatDate($racun->datum_racuna)}}</td>
					<td>{{AdminOptions::company_mesto()}},{{AdminNarudzbine::formatDate($racun->datum_racuna)}}</td>
					<td>{{AdminNarudzbine::formatDate($racun->rok_isporuke)}}</td>
					<td>{{AdminNarudzbine::formatDate($racun->datum_racuna)}}</td>
					<td>{{AdminNarudzbine::formatDate($racun->rok_placanja)}}</td>
					<td>{{$racun->nacin_placanja}}</td>
				</tr>
			</tbody>
		</table>
	</div>