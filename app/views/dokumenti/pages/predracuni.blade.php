@extends('dokumenti.templates.main')
@section('content')

<div id="main-content" class="kupci-page">
	<div class="flat-box predracuni-header">
		<div class="row flex"> 
			<div class="columns medium-3">  
				<form method="GET" action="{{DokumentiOptions::base_url()}}dokumenti/predracuni" class="columns medium-10 no-padd">
					<div class="flex relative">
						<input type="text" name="search" value="{{ urldecode($search) }}" placeholder="{{ AdminLanguage::transAdmin('Pretraga') }}..." class="m-input-and-button__input">
						<button class="btn btn-small btn-pon-pred" type="submit"><i class="fa fa-search"></i> </button>
						<!-- <a class="btn btn-small" href="{{DokumentiOptions::base_url()}}dokumenti/predracuni">{{ AdminLanguage::transAdmin('Poništi') }}</a> -->
					</div> 
				</form>
			</div>
			<div class="columns medium-3">
				<input type="radio" name="vrsta_dokumenta" class="JSVrstaDokumenta" value="ponuda"> <label class="no-margin">Ponude</label>
				<input type="radio" name="vrsta_dokumenta" class="JSVrstaDokumenta" value="predracun" {{($vrsta_dokumenta == 'predracun') ? 'checked' : ''}}> <label class="no-margin">Predračuni</label>
				@if(AdminOptions::gnrl_options(3050))
				<input type="radio" name="vrsta_dokumenta" class="JSVrstaDokumenta" value="racun" {{($vrsta_dokumenta == 'racun') ? 'checked' : ''}}> Racuni
				<input type="radio" name="vrsta_dokumenta" class="JSVrstaDokumenta" value="avansni_racun" {{($vrsta_dokumenta == 'avansni_racun') ? 'checked' : ''}}> Avansni racuni
				@endif
			</div>
			<div class="columns medium-2">
				<div class="m-input-and-button">
					<h2 class="no-margin"><label class="no-margin">Status</label></h2>&nbsp;
					<select id="JSStatusSearch">
						<option value="all">Svi statusi</option>
						{{ Dokumenti::statusSelect($dokumenti_status_id) }}
					</select>
				</div> 
			</div>
			<div class="columns medium-2">
				<div class="m-input-and-button">
					<h2 class="no-margin"><label class="no-margin">Kupac</label></h2>&nbsp;
					<select id="JSParnerSearch">
						<option value="0">Svi kupci</option>
						{{ Dokumenti::partnerSelect($partner_id) }}
					</select>
				</div> 
			</div>
			<div class="columns medium-2">
				<a href="{{DokumentiOptions::base_url()}}dokumenti/predracun/0" class="btn btn-primary btn-small">{{ AdminLanguage::transAdmin('Novi dokument') }}</a>
			</div>
		</div>
	</div>
	<div class="flat-box">
	 	<div class="row"> 
			<div class="columns medium-12 large-12">
				<label>{{ AdminLanguage::transAdmin('Ukupno') }}: {{ $count }}</label>
				<table>
					<tr>
						<th class="JSSort" data-sort_column="broj_dokumenta" data-sort_direction="{{ $sort_column == 'broj_dokumenta' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Broj Dokumenta') }}</th>
						<th class="JSSort" data-sort_column="naziv_partnera" data-sort_direction="{{ $sort_column == 'naziv_partnera' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Kupac') }}</th>
						<th class="JSSort" data-sort_column="datum_predracuna" data-sort_direction="{{ $sort_column == 'datum_predracuna' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Datum izdavanja') }}</th>
						<th class="JSSort" data-sort_column="vazi_do" data-sort_direction="{{ $sort_column == 'vazi_do' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Rok za plaćanje') }}</th>
						<th class="JSSort" data-sort_column="iznos" data-sort_direction="{{ $sort_column == 'iznos' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Ukupno') }} (din.)</th>
						<th></th>
					</tr>
					@foreach($predracuni as $row)
					<tr>
						<td>{{ $row->broj_dokumenta }}</td>
						<td>{{ $row->naziv_partnera }}</td>
						<td>{{ Dokumenti::dateInversion($row->datum_predracuna) }}</td>
						<td>{{ Dokumenti::dateInversion($row->vazi_do) }}</td>
						<td>{{ number_format($row->iznos,2,",",".") }}</td>
						<td><a href="{{DokumentiOptions::base_url()}}dokumenti/predracun/{{$row->predracun_id}}">{{ AdminLanguage::transAdmin('Izmeni') }}</a></td>
					</tr>
					@endforeach

				</table>
				{{ Paginator::make($predracuni,$count,$limit)->links() }}
			</div>
		</div>
	</div>
</div>
@endsection