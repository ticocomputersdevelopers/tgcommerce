
@if(!empty(All::getShortListNews()))
 	
 	<!-- INCLUDED IN HOME.BLADE -->
 
	<h2><a class="section-title text-center center-block" href="{{ Options::base_url() }}{{Url_mod::slug_trans('blog')}}">{{Language::trans(Support::title_blogs())}}</a>
	</h2> 

	<div class="JSblog-slick">   
		@foreach(All::getShortListNews() as $row) 
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="card-blog"> 
				@if(in_array(Support::fileExtension($row->slika),array('jpg','png','jpeg','gif')))

					<a class="bg-img center-block" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ $row->slika }}');"></a>

				@else

					<iframe src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   

				@endif

				<h3 class="blogs-title overflow-h">
					<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">{{ $row->naslov }}</a>
				</h3>

				<div class="blogs-date text-uppercase">
					{{ Support::date_convert($row->datum) }}
				</div>
			</div>
		</div>
		@endforeach 

	</div> 
 
@endif