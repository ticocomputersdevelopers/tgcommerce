
<!-- 			<div class="text-red">Artikal ima istoriju!</div> 
<div class="text-red">Unošenje novih artikala nije dozvoljeno!</div> -->
<div id="JSFAArticleForm" class="inner-modal">
	<input type="hidden" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
	<input type="hidden" name="clone_id" value="0">
	<input type="hidden" name="orgj_id" value="{{ $orgj_id }}">
	<input type="hidden" name="jedinica_mere_id" value="{{ $jedinica_mere_id }}">
	<input type="hidden" name="tarifna_grupa_id" value="{{ $tarifna_grupa_id }}">
 
<div class="row">
	<div class="col-md-5 col-sm-5 col-xs-12"> 
	 	<div class="naziv-web-big">
			<label>{{Language::trans('Naziv na webu')}}</label>
			<input type="text" name="naziv_web" value="{{ htmlentities($naziv_web) }}">
			<div id="JSFAnaziv_web_error" class="error"></div>
		</div>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-4"> 
	 	<div class="naziv-web-big">
			<label>{{Language::trans('SKU')}}</label>
			<input type="text" name="sku" value="{{ htmlentities($sku) }}">
			<div id="JSFAsku_error" class="error"></div>
		</div>
	</div>
	<div class="col-md-3 col-sm-2 col-xs-8"> 
		<div class="custom-dropdown">
			<label>{{Language::trans('Grupa')}}</label>
			<div class="custom-select-arrow"> 
				<input type="text" name="grupa_pr_grupa" value="{{ Language::trans($grupa_pr_grupa) }}" autocomplete="off">
			</div>
			<div class="short-group-container"> {{ Groups::listGroups() }} </div>
			<div id="JSFAgrupa_pr_grupa_error" class="error"></div>
		</div>
	 </div>
	<div class="col-md-2 col-sm-2 col-xs-12"> 
		<label>{{Language::trans('Stanje artikla')}}</label>
		<div class="custom-select-arrow"> 
			<input type="text" name="roba_flag_cene" value="{{ Language::trans($roba_flag_cene) }}" autocomplete="off">
		</div>
		<div class="custom-dropdown"> 
			<div class="short-group-container"> 
				<ul id="JSListaFlagCene" hidden="hidden">
					@foreach(Support::getFlagCene() as $row)
						<li class="JSListaFlagCena" data-roba_flag_cene="{{ Language::trans($row->naziv) }}">{{ Language::trans($row->naziv) }}</li>
					@endforeach
				</ul>
			</div>
		</div>
		<div id="JSFAroba_flag_cene_error" class="error"></div>
	</div>
 </div>
 <br>
 <div class="row custom-col">
	<div> 
		<label>{{Language::trans('Na Webu')}} </label><br>
		<div class="custom-select-arrow"> 
			<select name="flag_prikazi_u_cenovniku">
				@if($flag_prikazi_u_cenovniku)
				<option value="1" selected>{{Language::trans('DA')}}</option>
				<option value="0" >{{Language::trans('NE')}}</option>
				@else
				<option value="1" >{{Language::trans('DA')}}</option>
				<option value="0" selected>{{Language::trans('NE')}}</option>
				@endif
			</select>
		</div>
	</div>	
    <div> 
		<label>{{Language::trans('Akcija')}}</label><br>
		<div class="custom-select-arrow"> 
			<select name="akcija_flag_primeni" id="JSAkcijaFlagPrimeni">
				@if($akcija_flag_primeni)
				<option value="1" selected>{{Language::trans('DA')}}</option>
				<option value="0" >{{Language::trans('NE')}}</option>
				@else
				<option value="1" >{{Language::trans('DA')}}</option>
				<option value="0" selected>{{Language::trans('NE')}}</option>
				@endif
			</select>
		</div>
	</div>
	<div class="relative"> 
		<label>{{Language::trans('Tip artikla')}}</label>
		<div class="custom-select-arrow"> 
			<input type="text" name="tip_artikla" value="{{ Language::trans($tip_artikla) }}" autocomplete="off">
		</div>
		<div class="custom-dropdown"> 
			<div class="short-group-container"> 
				<ul id="JSListaTipaArtikla" hidden="hidden">
					<li class="JSListaTipArtikla" data-tip_artikla="">{{Language::trans('Bez tipa')}}</li>
					@foreach(Support::getTipovi() as $row)
					<li class="JSListaTipArtikla" data-tip_artikla="{{ Language::trans($row->naziv) }}">{{Language::trans($row->naziv)}}</li>
					@endforeach
				</ul>
			</div>
		</div>
		<div id="JSFAtip_artikla_error" class="error"></div>
	</div>
    <div class="relative">  
		<label>{{Language::trans('Web cena')}}</label>
		<input type="text" name="web_cena" class="JSCenaChange" value="{{ $web_cena }}" {{ Product::vrsteCena('WEB') ? '' : 'disabled' }}>
		<div id="JSFAweb_cena_error" class="error"></div> 
	</div>
	<div class="relative">  
		<label>{{Language::trans('Akcijska cena')}}</label>
		<input type="text" name="akcijska_cena" id="JSAkcijskaCena" value="{{ $akcijska_cena }}" {{ Product::vrsteCena('NC') ? '' : 'disabled' }}> 
	</div> 
	<div class="relative">
		<label>{{Language::trans('Količina')}}</label>
		<input type="text" name="kolicina" value="{{ $kolicina }}">
		<div id="JSFAkolicina_error" class="error"></div>
	</div> 
 </div>
 
    <div class="short-product-action row" id="JSAkcijaDatum" {{ $akcija_flag_primeni == 0 ? 'hidden' : '' }}><br>
		<div class="col-md-3">
			<label>{{Language::trans('Akcija traje od')}}:</label>
			<input class="akcija-input" id="datum_akcije_od" name="datum_akcije_od" autocomplete="off" type="text" value="{{ $datum_akcije_od }}">
			<span id="datum_od_delete" class="date-delete">&times;</span>
		</div>
		<div class="col-md-3">
			<label>{{Language::trans('Do')}}:</label>
			<input class="akcija-input" id="datum_akcije_do" name="datum_akcije_do" autocomplete="off" type="text" value="{{ $datum_akcije_do }}">
			<span id="datum_do_delete" class="date-delete">&times;</span>
		</div>
	</div>

	<div class="row"><br>
		<div class="col-md-12">
			<h5 class="info-heading">&nbsp; {{Language::trans('Slike')}}</h5> 
			@if(!empty(Product::design_id($roba_id)) AND Options::pitchprint_aktiv() == 1)    
				<div class="modal-article-pic">
					<img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766">
				</div> 
				@if(Product::pitchPrintImageExist($roba_id) == TRUE) 
				<div class="modal-article-pic">
					<img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766">	
				</div>                         
				@endif
            @else
			@foreach(Product::getSlike($roba_id) as $row)
			<div class="modal-article-pic"> 
				<div class="clearfix"> 
					<span class="tooltipz" aria-label="{{Language::trans('Glavna slika')}}"> 
					<input type="radio" name="akcija" class="akcija" value="{{ $row->web_slika_id }}" @if($row->akcija) checked @endif> </span>
					<span class="JSDeleteSlika pull-right tooltipz" data-roba_id="{{ $roba_id }}" data-web_slika_id="{{$row->web_slika_id}}" aria-label="{{Language::trans('Obriši')}}"><i class="fa fa-close"></i></span>
				</div>
				<img class="img-responsive" src="{{ Options::domain().$row->putanja }}">
			</div>
			@endforeach
			@endif
			<div id="JSUploadImages" class="column medium-12 short-article-img">		 
				<div class="custom-file-input relative"> 
					<span class="inline-block">{{Language::trans('Dodaj sliku')}}</span>
					<input type="file" name="slike[]" multiple>
				</div> 
				<div id="JSSlikaError" class="error"></div>		 
			</div>
		</div>
	</div>
	<div class="row"><br>
		<div class="col-md-12"> 
			<h5 class="info-heading">&nbsp; {{Language::trans('Opis')}}</h5> 
			<textarea id="JSFAweb_opis" name="web_opis">{{ $web_opis }}</textarea>
		</div>
	</div>
	<div class="row"><br>
		<div class="col-md-12 modal-btns text-center"> 
			<button id="JSFAArticleSubmit" class="btn">{{Language::trans('Sačuvaj')}}</button> 
			@if($roba_id != 0)
			<button id="JSFAArticleDelete" class="btn delete-me" data-roba_id="{{$roba_id}}">{{Language::trans('Obriši artikal')}}</button>
			@endif
			<a href="{{Options::domain()}}admin/product/{{$roba_id}}" class="btn">{{Language::trans('Detaljnija izmena')}} </a>
			@if($roba_id!=0)
			<a href="{{Product::article_link($roba_id)}}" target="_blank" class="btn">{{Language::trans('Vidi artikal')}} </a>    
			@endif
		</div>
	</div>
</div>
 